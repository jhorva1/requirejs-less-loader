# RequireJS Less Loader

Custom plugin for RequireJS that allows for loading less. Once it is parsed, it is added to the DOM (to the end of head element) and passed as object in once the module loads if any changes need to be made after loading.

### How to use it

What you need first is the less script which you can get from [here](https://github.com/less/less.js/blob/master/dist/less.min.js).
Next, if you don't already have text plugin for require, get it [here](https://github.com/requirejs/text/blob/master/text.js).

You can place all of the javascript files inside the scripts folder.

### Simple example

##### index.html

```HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Less Test</title>
</head>
<body>
    <script src="scripts/less.min.js"></script>

    <!-- Require.js -->
    <script data-main="scripts/main.js" src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.6/require.min.js"></script>
</body>
</html>
```

##### index.less

```less
p{
    color: red;
}
```

##### scripts/main.js

```JavaScript
define([
    'text!../index.html',
    'less!../index'
], function(html, less){
    'use strict';

    console.log(html, less);
});
```