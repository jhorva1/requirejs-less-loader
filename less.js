define({

    load: function (moduleName, parentRequire, onLoad, config) {

        var theModule = moduleName.split('/').pop();

        if(!theModule.includes('.')){
            moduleName += '.less';
        }

        var path = 'text!' + moduleName;

        parentRequire([path], function (lessText) {

            less.render(lessText).then(function (output) {
                var style = document.createElement('style');
                style.type = 'text/css';

                if (style.styleSheet) {
                    style.styleSheet.cssText = output.css;
                } else {
                    style.appendChild(document.createTextNode(output.css));
                }

                document.getElementsByTagName("head")[0].appendChild(style);

                onLoad(style);
            }, function (error) {
                if (console && console.error) {
                    console.error(error);
                }

                onLoad(false);
            });

        });
    }
});